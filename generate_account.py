from luhn import *

# count=0
#
# for i in range(0,99999999):
#     if verify(str(i)) == True:
#         count +=1
#         print(i)
#
# if verify("999996"):
#     print("this is valid")
# else:
#     print("this is not valid")
#
# print("the max number of numbers in 1M: {}".format(count))

# Python program to illustrate the concept
# of threading
# importing the threading module
import threading


def generate_number(threadName,first, end):
    """
    function to print cube of given num
    """
    count=0
    for i in range(first,end):
        if verify(str(i)) == True:
            count +=1
            # print("{}: {}".format(threadName,i))

    print("Total: {} \n".format(count))


if __name__ == "__main__":
    # creating thread
    t1 = threading.Thread(target=generate_number, args=("t1",1,1000000, ))
    t2 = threading.Thread(target=generate_number, args=("t2",1000001,2000000,))
    t3 = threading.Thread(target=generate_number, args=("t3", 2000001, 3000000,))
    t4 = threading.Thread(target=generate_number, args=("t4", 3000001, 4000000,))
    t5 = threading.Thread(target=generate_number, args=("t5", 4000001, 5000000,))


    # starting thread 1
    t1.start()
    # starting thread 2
    t2.start()

    t3.start()
    t4.start()
    t5.start()

    # wait until thread 1 is completely executed
    t1.join()
    # wait until thread 2 is completely executed
    t2.join()
    t3.join()
    t4.join()
    t5.join()

    # both threads completely executed
    print("Done!")
